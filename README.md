# Feature Learning with Contrastive Predictive Coding

[https://arxiv.org/pdf/1807.03748.pdf](https://arxiv.org/pdf/1807.03748.pdf)

# Struktur
- cpc: Alle wichtigen Klassen
- trained_models: Vortrainierte Netzwerke. Details finden sich in den Notebooks.
- notebooks (**Pfade in den Notebooks müssen angepasst werden!**):
    - ```cpc_train.ipynb```: Trainigsdurchlauf der Implementierung auf Imagenet Daten
    - ```autoencoder_train.ipynb```: Trainigsdurchlauf für den Vergleich mit einem Autoencoder
    - ```imagenet_selection.ipynb```: Selektion einer Auswahl an Labels aus dem Imagenet Datensatzes
- tests: Enthält eine langsame Referenzimplementierung zum validieren der Arbeitsweise, sowie einen Autoencoder 
zum Validieren der Encoding Qualität des CPC Netzwerkes


## Feature Learning with Contrastive Predictive Coding

[https://arxiv.org/pdf/1807.03748.pdf](https://arxiv.org/pdf/1807.03748.pdf)


## Einleitung 


*   **Contrastive**: Trainiert wird, indem das Modell zwischen einer richtigen und mehreren falschen, vorgegebenen Sequenzen unterscheiden muss.
*   **Predictive**: Das Modell soll die Zukunft anhand eines gegebenen Kontextes vorhersagen.
*   **Coding**: Die Vorhersagen finden im "latenten" Raum statt, d.h. mit einer reduzierten Variante der zugrundeliegenden Daten.

_Contrastive Predictive Coding_ ist eine Methode, mit der man unsupervised sinnvolle Features aus Daten lernen kann. 

Beispielsweise zeigen Bienen Sozialverhalten, welches sich in verschiedene Kategorien einteilen lässt. Um das Verhalten der Bienen zu klassifizieren, ist es hilfreich, aus den Videoaufnahmen der Bienen diesen Kategorien zuordenbare Strukturen extrahieren zu können.

Um dies mit einem supervised Modell zu bewerkstelligen, müsste ein großer Trainings- Datensatz vorhanden sein, bei dem die zu sehenden Verhaltensmuster vorher notiert wurden. Dieser Datensatz existiert bis jetzt nicht und ist von Hand nur sehr mühsam zu erstellen. Daher ist der alternative Ansatz, "irgendeine" sehr aussagekräftige, aber reduzierte Darstellung der Daten zu finden, in der sich dann hoffentlich Verhaltensmuster leichter ablesen lassen (beispielsweise durch gezieltes Clustering).


## Vorbetrachtung: Übersicht Paper

Das oben genannte Paper beschreibt Contrastive Predictive Learning (CPC), ein Framework, das im Rahmen von unsupervised learning verwendet wird. Es soll in diesem Zusammenhang helfen, nützliche Repräsentationen hochdimensionaler Daten zu erzeugen. Insbesondere liegt der Schwerpunkt hierbei darauf, dass das Modell auf ganz verschiedene Probleme angewendet werden kann. So zeigen sich laut der Autoren sehr gute Ergebnisse in den Bereichen Sprache, Text, Bilder sowie Reinforcement learning.

Das Ziel unseres Projektes ist es, die für Bilder beschriebene Architektur des Papers ([https://arxiv.org/pdf/1807.03748.pdf](https://arxiv.org/pdf/1807.03748.pdf)) zu implementieren. Zum einen soll der Aufwand dieser Implementierung abgeschätzt werden. Zum anderen könnte diese Methode, wie oben beschrieben, zukünftig dazu dienen, Features aus Bildern und Videos ähnlich der Ausschnitte in Abbildung 1, zu extrahieren, um damit Rückschlüsse auf das Verhalten der Bienen ziehen zu können.

![alt_text](images/image1.gif "image_tooltip")


**Abbildung 1:** Videoaufnahmen aus einem Bienenstock


## Implementierung


### Methode

Die grundlegende Idee der Implementierung von CPC für Bilder besteht darin, die Bilder jeweils in überlappende Teile (Patches) zu unterteilen. Diese werden anschließend als Sequenz verwendet und können genutzt werden, um kommende Patches eines Bildes vorherzusagen.


#### Im Paper

256x256 px Bilder der ImageNet Datenbank werden in Patches zerlegt. Anschließend werden, ausgehend von einem aktuellem Patch,_ k_ zukünftige Patches anhand separater Gewichte vorhergesagt.


#### Unsere Implementierung

Aufgrund begrenzter Rechenkapazitäten nutzen wir auf 64x64 px skalierte Bilder aus dem ImageNet Datensatz. Zudem haben wir den Datensatz aus Performancegründen von 1000 verschiedenen Labels auf die 100 häufigsten Labels reduziert.

Diese zerlegen wir in Patches der Größe 16x16 px, die sich halb überlappen. Siehe dazu Abbildung 2. Anschließend sollen auch hier _k_ zukünftige Patches ausgehend von einem aktuellen Patch vorhergesagt werden.


![alt_text](images/image3.png "image_tooltip")


**Abbildung 2:** Datengrundlage bilden Bilder des ImageNet Datensatzes. Diese werden der Reihe nach in 16x16 px Patches zerlegt. Die Patches überlagern sich zu 50%.


### Netzwerk

Das Netzwerk benötigt zwei Kernkomponenten. Die erste wird zum Kodieren der Bilder verwendet, sodass die hochdimensionalen Daten auf weniger Dimensionen reduziert werden können. Dadurch wird die weitere Verarbeitung der Daten und letztendlich die Berechnung der benötigten Vorhersagen vereinfacht. Hierfür kann prinzipiell jede Art von Encoder eingesetzt werden.

Zum anderen wird eine Komponente zur Vorhersage künftiger Daten benötigt. Diese soll in der Lage sein, aus einer gegebenen Sequenz einen Kontext zu bestimmen, der eine Vorhersage künftiger Daten ermöglicht. In unserem Fall wird die Sequenz aus den aufeinanderfolgenden Patches eines Bildes gebildet und der gelernte Kontext soll es ermöglichen, die nächsten Patches vorherzusagen. Hierfür kommen sogenannte autoregressive Modelle zum Einsatz, die eine Art Gedächtnis besitzen (siehe Abbildung 3).


#### Im Paper

Als Encoder wird ein _ResNet-v2-101_ verwendet, welches die Patches  jeweils auf einen 1024-d Vektor projiziert. Anschließend kommt ein PixelCNN zum Einsatz, das zum Lernen des Kontextes und zur Vorhersage der Patches dient.


#### Unsere Implementierung

Als Encoder verwenden wir ein leicht abgewandeltes Resnet18 von Pytorch, indem die Batch Normalisierung zwischen den einzelnen Layern entfernt wird, um ein besseres Lernen des Modells zu erreichen.

Die Embeddings der Patches werden dann an eine GRU von Pytorch übergeben, die den Kontext der Patches lernen soll. 


![alt_text](images/image8.png "image_tooltip")


**Abbidlung 3:** Das Netzwerk besteht aus zwei Komponenten, einem Encoder (genc), der die Repräsentationen der Patches erzeugt, und einem autoregressiven Netzwerk (gar), das den Kontext einer Sequenz lernen kann. Anhand der gelernten Vergangenheit sollen zukünftige (Repräsentationen der) Patches vorhergesagt werden. 

_Quelle: [https://arxiv.org/pdf/1807.03748.pdf](https://arxiv.org/pdf/1807.03748.pdf)_


### Training

Abschließend wird das komplette Modell trainiert, indem anhand der aktuellen Vorhersage der zum jeweiligen Zeitpunkt auftretende Fehler des Modells berechnet wird. Dieser wird anschließend verwendet, um die Parameter des Modells anzupassen und damit die Vorhersage zu verbessern.

Die hierbei verwendete Loss-Funktion, die den Fehler berechnen soll, basiert auf Noise-Contrastive-Estimation: Das Modell soll lernen, zwischen richtigen, zukünftigen Teilen der Sequenz (also dem nächsten Patch eines Bildes) und falschen, zufällig gewählten Teilen zu unterscheiden. Als Grundlage für diese Entscheidung dienen die vorhergehenden Patches (die Vergangenheit im Sinne einer Sequenz). Damit wird die so genannte "mutual information" zwischen den Patches eines Bildes maximiert, wodurch die Kerneigenschaften des Bildes eingefangen werden sollen. Mutual Information ist hierbei eine allgemeine Definition für die Korrelation zweier Größen, in unserem Fall die Vergangenheit und Zukunft einer Patch-Sequenz.

Da das Netzwerk bestehend aus dem kodierenden und vorhersagenden Teil als Ganzes trainiert wird, lernt der Encoder im Laufe des Prozesses eine Darstellung, in der sich inhärente Eigenschaften des Bildes wiederspiegeln.


#### Im Paper

Das Training folgt dem oben beschriebenen Konzept. Als Optimierer kommt Adam mit einer Lernrate von 2e-4 zum Einsatz. Es wird mit einer Batch Size von 16 auf 32 GPUs trainiert.


#### Unsere Implementierung

Auch bei uns erfolgt das Training nach der beschriebenen Methode, auch wir verwenden Adam als Optimierer. Die Batch Size wählen wir mit 10, die Lernrate als 0.001. Trainiert wird auf einer GPU.


### Test


#### Im Paper

Nach einem Mean-Pooling der durch das ResNet kodierten Repräsentationen wird ein Linearer Classifier auf diesen trainiert. Als Optimierer wird SGD mit Momentum verwendet.


#### Unsere Implementierung

Nach dem Training des CPC-Modells  nutzen wir die vom ResNet erstellten Embeddings und trainieren ein einfaches Lineares Netz auf diesen. 

Zum Vergleich wird ein einfacher Autoencoder auf den Patches trainiert und anschließend auch hier die Performance eines einfachen Linearen Netzes auf den entstehenden Daten getestet. 


## Ergebnisse

Die Evaluierung nach der oben beschriebenen Methode hat ergeben, dass nach einer Kodierung der Patches mit einem einfachen Autoencoder, die Genauigkeit des Linearen Classifiers nach dem Training auf allen Daten bei 1.0% liegt.

Die Kodierung des ResNets des CPC mit anschließender linearer Klassifikation führte hingegen zu einer Genauigkeit des Classifiers von 1.9%.


## Diskussion

![alt_text](images/cpc_loss.png "image_tooltip")


**Abbildung 4:** Verlauf der Loss Funktion des CPC-Netzwerkes während des Lernens.

Nach Abschluss unseres Projektes bieten sich noch einige Möglichkeiten an, die dazu dienen könnten unsere CPC-Implementierung zu verbessern. 

Ein Hauptproblem stellt derzeit die Ausführungsgeschwindigkeit der Implementierung dar. Wenn die Implementierung in Zukunft in diese Richtung verbessert wird, können viele weitere Verbesserungen, die bei derzeitigem Stand zu viel Zeit in Anspruch nehmen würden, angegangen werden.

Hierzu zählt, die Hyperparameter des Modells zu untersuchen. Diese wurden bisher keiner Evaluierung unterzogen und wurden auch im Paper nicht beschrieben. Daher gehen wir davon aus, dass durch eine eventuell geeignetere Wahl der Parameter eine signifikante Verbesserung der Ergebnisse des Modells möglich sein könnte. Ansatzpunkte wären hier beispielsweise die Wahl der Anzahl der Hidden Units innerhalb der GRU sowie der Lernrate. Auch der Einsatz eines PixelCNNs statt der GRU wäre denkbar.

Außerdem deutet der in Abbildung 4 zu sehende Verlauf der Loss-Funktion darauf hin, dass ein längerer Lernprozess den Loss weiter verringern würde.

Nach Umsetzung einiger Verbesserungen wäre zukünftig auch die angedachte Anwendung unseres Modells auf die Daten zum Bienenverhalten denkbar.
