"""
This module contains classes and functions
that are used to test and verify the implementation.

"""
import random
import torch
import torch.nn as nn
from torch.nn.modules.loss import _Loss


class ReferenceInfoNCE(_Loss):
    """
    Reference low performance implementation.

    """

    def __init__(self, n_predict, n_negatives, enc_size, hidden_size, device=None):
        super().__init__()
        self.device = device
        self.n_predict = n_predict  # Number of future steps
        self.n_negatives = n_negatives  # Max-Number of negatives

        # Linear transformation matrix. One for each step
        self.W = nn.Parameter(data=torch.randn([n_predict, enc_size, hidden_size],
                                               dtype=torch.float64).type(torch.FloatTensor) * 1 / enc_size,
                              requires_grad=True)

    def single_series(self, z_k, z_k_negatives, k, c_t):
        """
        Parameters
        ----------
        k: int
            Time is t+k
        z_k: Tensor
            Positive z at k
        z_k_negatives: List of Tensors
            Negative z at k

        """
        neg_loss = 0
        for z in z_k_negatives:
            neg_loss += self.f_k(z, k, c_t)

        correct = self.f_k(z_k, k, c_t)
        all_negatives = neg_loss

        quot = correct / (all_negatives + correct)

        return -1 * torch.log(quot)

    def forward(self, enc_patches, output):
        losses = []
        for batchnr, c_ts in enumerate(output):
            for t, c_t in enumerate(c_ts):
                for k in range(1, self.n_predict + 1):
                    try:
                        z_k = enc_patches[batchnr][t + k]
                    except IndexError:
                        break
                    z_k_negatives = self.sample_negatives(enc_patches[batchnr], self.n_negatives, k)
                    loss = self.single_series(z_k, z_k_negatives, k, c_t)
                    losses.append(loss)

        return torch.stack(losses).mean()

    def f_k(self, z_k, k, c_t):
        w_ct = torch.mv(self.W[k - 1], c_t)
        z_w = torch.dot(z_k, w_ct)
        y = torch.exp(z_w.type(torch.DoubleTensor))

        if torch.isnan(y) or y == float("Inf"):
            with torch.no_grad():
                print('ohoh: exp({})'.format(z_w.item()))
                print("W: ", self.W[k - 1].max())
                print("c_t: ", c_t.max())
                print("z_k: ", z_k.max())

        return y

    @staticmethod
    def sample_negatives(patches, n_negatives, k):
        """
        Draws n_negative random, unequal samples from patches.

        It excludes patch[k].

        Parameters
        ----------
        patches: Tensor
            Tensor to draw samples from
        n_negatives: Int
            Number of samples drawn
        k: Int
            Index of patches to be excluded

        Returns
        -------
        Tensor: len() == n_negatives
            Random samples

        """
        samples = []
        hist = []

        while len(samples) < n_negatives:
            i = random.randint(0, len(patches) - 1)

            if i not in hist and i != k:
                samples.append(patches[i])
                hist.append(i)

        return torch.stack(samples)


class autoencoder(nn.Module):
    """
    Simple convolutional autoencoder

    Result of encoding has dims 4, 8, 8.


    """
    def __init__(self):
        super().__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(3, 16, 3, stride=1, padding=1),
            nn.ReLU(True),
            nn.MaxPool2d(2, stride=2),
            nn.Conv2d(16, 4, 3, stride=2, padding=1),
            nn.ReLU(True),
            nn.MaxPool2d(2, stride=2)  # b, 4, 8, 8
        )
        self.decoder = nn.Sequential(
            nn.ConvTranspose2d(4, 16, 3, stride=2),  # b, 16, 5, 5
            nn.ReLU(True),
            nn.ConvTranspose2d(16, 8, 5, stride=2, padding=2),  # b, 8, 15, 15
            nn.ReLU(True),
            nn.ConvTranspose2d(8, 3, 4, stride=2, padding=2),  # b, 1, 28, 28
            nn.Tanh()
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x
