"""
This module contains the InfoNCE Loss function
used in the Contrastive Predictive Coding paper.


"""
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.modules.loss import _Loss


class simpleCNN(nn.Module):
    """
    Very simple CNN.

    But it works.

    It might be used as an encoder within the CPC-proposed network layout.

    """
    def __init__(self):
        super().__init__()
        # RGB Bilder: 3 Lagen
        # erst 6 Filter, dann 16 Filter, alle jeweils 3x3
        self.conv1 = nn.Conv2d(3, 6, 3)
        self.conv2 = nn.Conv2d(6, 16, 3)

        # zwei fully connected layer (14x14 ist Größe der Feature Map bei gegebenen Filtern)
        self.fc1 = nn.Linear(64, 2000)
        self.fc2 = nn.Linear(2000, 1000)

    def forward(self, x):
        # Max pooling over a (2, 2) window -> Horizontale und Vertikale werden halbiert
        # Output Shape des Conv Netzes: Filter x wie oft Filter passt x wie oft Filter passt
        x = self.conv1(x)
        x = F.max_pool2d(F.relu(x), 2)
        x = F.max_pool2d(F.relu(self.conv2(x)), 2)
        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        x = self.fc2(x)

        return x

    @staticmethod
    def num_flat_features(x):
        # Size gibt Dimensionen als array zurück, die sollen alle multipliziert werden, außer die batch size
        size = x.size()[1:]
        num_features = 1
        for s in size:
            num_features *= s
        return num_features


class InfoNCE(_Loss):
    """
    Implements the so called InfoNCE loss function.

    This loss is especially implemented on image-patches.

    The loss is calculated by using one positive (eg. correct) future patch
    and n_negatives negative (eg. wrong, random sampled) future patches.

    These patches are linearly transformed by using a unique transformation
    matrix for each prediction step k.

    Afterwards, the loss is calculated using a Logsoftmax function.

    """

    def __init__(self, n_predict, n_negatives, enc_size, hidden_size, same_image=True, device=None):
        """

        Parameters
        ----------
        n_predict: Int
            Number of future image patches to be predicted
        n_negatives: Int
            Number of negative samples for contrastive loss calculation
        enc_size: Int
            Number of dimensions the internal encoder produces
        hidden_size: Int
            Number of hidden dimensions of the RNN
        same_image: bool
            Indicates, if the negative sample should be drawn from the same images or from
            all images in a batch
        device: Cuda Device
        """
        super().__init__()
        self.device = device
        self.n_predict = n_predict  # Number of future steps
        self.n_negatives = n_negatives  # Max-Number of negatives
        self.logsoftmax = torch.nn.LogSoftmax(dim=1)
        self.same_image = same_image    # Random sampling from same image for negative patches

        # Linear transformation matrix. One for each step
        self.W = nn.Parameter(data=torch.randn([n_predict, enc_size, hidden_size],
                                               dtype=torch.float64).type(torch.FloatTensor) * 1 / enc_size,
                              requires_grad=True)
        torch.nn.init.orthogonal_(self.W)

    def single_series(self, z_k, z_k_negatives, c_t, _n_predict):
        """
        Parameters
        ----------
        z_k: Tensor
            Positive z for all t+k, k=0 to k=N_predict
            shape: (N_predict, enc_size)

        z_k_negatives: List of Tensors
            Negative z at k
            shape: (N_negative, N_predict, enc_size)

        c_t: Tensor
            Context Tensor
            shape: (hidden_size)

        _n_predict: Int
            Possible future steps without padding

        """
        correct = self.f_k(z_k, c_t)[:_n_predict, None]
        all_negatives = self.f_k(z_k_negatives, c_t)[:_n_predict]   # Shape: (N_predict, N_negatives, enc_size)

        loss = -1 * self.logsoftmax(torch.cat((correct, all_negatives), dim=1))[:, 0]

        return loss

    def forward(self, enc_patches, output):
        """


        Parameters
        ----------
        enc_patches: Tensor
            shape (batch, h*w, enc_size)
        output: Tensor
            shape (batch, h*w, hidden_size)

        Returns
        -------
        Loss: Tensor-Item

        """
        losses = []
        patches_per_image = enc_patches.shape[1] - 1

        for image_nr, c_ts in enumerate(output):
            for t, c_t in enumerate(c_ts):
                # Shift window for sampling z_ks
                enc_patches = torch.roll(enc_patches, -1, dims=1)

                if t == len(c_ts) - 1:
                    break

                # No padding when calculating future.
                # Calc distance to last patch
                dist = patches_per_image - t
                _n_predict = min(dist, self.n_predict)

                # True forecast
                z_k = enc_patches[0, :self.n_predict]

                # n_negative negative forecasts for each time step
                z_k_negatives = self._create_negative_samples(enc_patches)

                losses.append(
                    self.single_series(z_k, z_k_negatives[:, :self.n_predict], c_t, _n_predict)
                )

            enc_patches = torch.roll(enc_patches, -1, dims=0)

        return torch.cat(losses, dim=0).mean()

    def f_k(self, z_k, c_t):
        """
        Calculates score for n z_k vectors with the same context vector.

        f = z x W x c_t

        Scores are used to calculate the loss.

        Parameters
        ----------
        z_k: tensor
            shape:(N_predict, enc_size) | (N_negative, N_predict, enc_size)
        c_t: tensor
            shape:(hidden_size)

        Returns
        -------
        tensor
            shape: (N_predict) | (N_predict, N_negative)
        """
        if len(z_k.shape) == 3:
            # All negative samples for all future time steps
            return torch.einsum('gij, ijh, h -> ig', z_k, self.W, c_t)
        else:
            # Positive samples for all future time steps
            return torch.einsum('ij, ijh, h -> i', z_k, self.W, c_t)

    @staticmethod
    def _shuffle_tensor(t):
        """
        Shuffles all elements of a tensor and keeps dims.

        Parameters
        ----------
        t: Tensor

        Returns
        -------
        Tensor

        """

        one_dim = t.reshape(-1)
        idmax = len(one_dim)

        # Permute Indizes
        new_index = torch.randperm(idmax, device=t.device)

        # Reindex the Tensor with permuted indizes
        shape_before = t.shape

        return torch.index_select(one_dim, 0, new_index).reshape(shape_before)

    def _create_negative_samples(self, enc_patches):
        """

        Parameters
        ----------
        enc_patches: Tensor
            shape (batch, h*w, enc_size)

        Returns
        -------
        zk_negatives: Tensor
            shape (N_negatives, h*w, enc_size)

        """
        if self.same_image:
            # Create negatives
            # Take correct image patches
            z_k_negatives = enc_patches[0]

            # Expand to number negatives wanted
            z_k_negatives = z_k_negatives.expand((self.n_negatives, *z_k_negatives.shape))
        else:
            z_k_negatives = enc_patches[:self.n_negatives]

        # Shuffle
        z_k_negatives = self._shuffle_tensor(
            z_k_negatives
        )

        return z_k_negatives


def fix_resnet(resnet):
    """
    Fix for current torchvision's resnets, that do not use
    AdaptiveAvgPool2d.

    Parameters
    ----------
    resnet: instance of torchvision.models.resnet

    Returns
    -------
    resnet

    """
    resnet.avgpool = nn.AdaptiveAvgPool2d((1, 1))
    return resnet
