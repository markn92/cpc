"""
For implementing the CPC Network there is the need
for a resnet without batch normalization.

Therefore this module contains a modified version of the
official pytorch resnet classes.

"""
import torch
import torch.nn as nn
from torch.nn.utils import weight_norm
import math
import torch.utils.model_zoo as model_zoo

__all__ = ['ResNet', 'resnet18']

model_urls = {
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
}


def conv3x3(in_planes, out_planes, stride=1):
    """3x3 convolution with padding"""
    return weight_norm(nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                                 padding=1, bias=False))


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = noBatchnorm(planes)
        self.selu = nn.SELU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        # torch.nn.init.normal_(self.conv2.weight, std=0.001)
        self.bn2 = noBatchnorm(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        # out = self.bn1(out)
        out = self.selu(out)

        out = self.conv2(out)
        # out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.selu(out)

        return out


class ResNet(nn.Module):
    """
    Custom Resnet for CPC.

    Less layers and no batch normalization between the layers.

    Heavily inspired by torchvision.models.resnet

    """

    def __init__(self, block, layers, num_classes=1000):
        self.inplanes = 64
        super(ResNet, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(num_classes, affine=False)
        self.selu = nn.SELU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(256 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False)
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.selu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        # Leave after layer 3
        # x = self.layer4(x)

        x = self.avgpool(x)
        x = self.bn1(x)
        x = x.view(x.size(0), -1)
        # x = self.fc(x)

        return x


def resnet18(pretrained=False, **kwargs):
    """Constructs a ResNet-18 model.

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = ResNet(BasicBlock, [2, 2, 2, 2], **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet18']))
    return model


class noBatchnorm(nn.Module):

    def __init__(self, num_features):
        super().__init__()
        self.num_features = num_features
        self.mu = nn.Parameter(data=torch.Tensor(num_features))
        self.sigma = nn.Parameter(data=torch.Tensor(num_features))
        self.data_init = True

    def forward(self, input):
        """

        Parameters
        ----------
        input: torch.Tensor

        Returns
        -------

        """
        if self.data_init:
            self.mu.data = input.permute(0, 2, 3, 1).reshape(-1, self.num_features).mean(0)
            self.sigma.data = input.permute(0, 2, 3, 1).reshape(-1, self.num_features).std(0)

            self.data_init = False

        # self.mu and self.sigma have shape:    [self.num_features]
        # input has shape:                      [batch, self.num_features, height, width]

        input_shape = input.shape  # Keep for later

        # Merge height and width
        # input shape is now:                   [batch, self.num_features, height * width]
        _input = input.view(input.shape[0], input.shape[1], -1)

        # Increase dimensions of mu and sigma those of _input
        _mu = self.mu.unsqueeze(0).unsqueeze(2).expand(_input.shape[0], -1, _input.shape[2])
        _sigma = self.sigma.unsqueeze(0).unsqueeze(2).expand(_input.shape[0], -1, _input.shape[2])

        # Restore input's shape
        input = ((_input - _mu) / _sigma).view(input_shape)

        # print("new mean: ", input.permute(0, 2, 3, 1).reshape(-1, self.num_features).mean(0))
        # print("new std: ", input.permute(0, 2, 3, 1).reshape(-1, self.num_features).std(0))

        return input
