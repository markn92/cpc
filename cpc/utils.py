"""
Tools for data processing and common training/validation tasks
on ImageNet Data.

"""
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
from torch.utils.data import Dataset
from sklearn.feature_extraction.image import extract_patches


class PictureDataset(Dataset):
    """
    Wrapper for the hd5-stored imagenet Database.

    """

    def __init__(self, hd5file, transform=None, index_map=None, label_map=None):
        """
        Parameters
        ----------
        hd5file: hd5file containing images
        transform (callable, optional): Optional transform to be applied
            on a sample.
        index_map: List or Array
            Maps an index to another. Useful for subsets in combination with label_map
        label_map: Dict
            Maps a label to another label. If using subsets, this can be used to assure
            that the labels are properly arranged between a min and max value.

        """
        self.hd5file = hd5file
        self.transform = transform
        self.index_map = index_map
        self.label_map = label_map

    def __len__(self):
        if self.index_map is None:
            return len(self.hd5file['images'])
        else:
            return len(self.index_map)

    def __getitem__(self, idx):
        if self.index_map is not None:
            idx = self.index_map[idx]

        image = self.hd5file['images'][idx].T
        label = self.hd5file['targets'][idx]

        if self.label_map is not None:
            label = np.int64(self.label_map[label])

        sample = {'image': image / 255 * 2 - 1, 'label': label.astype('int64')}

        if self.transform:
            sample = self.transform(sample)

        return sample


class extractPatches(object):
    """
    This class extracts half-overlapping patches from
    color images.

    """

    def __init__(self, patch_size):
        """

        Parameters
        ----------
        patch_size: 2D-Tuple
            Two dimensional size of the patches that shall be extracted.

        """
        self.patch_size = patch_size
        self.extraction_step = patch_size[0] // 2

    def __call__(self, sample):
        image = sample['image']

        # Color-Dimensions must be progressed separately and later joined
        patches = []
        for channel in image:
            channel = extract_patches(channel, self.patch_size, self.extraction_step)
            patches.append(channel)

        patches = np.array(patches)
        patches = patches.transpose((1, 2, 0, 3, 4))

        return {'image': patches, 'label': sample['label']}


class EncodePatches(object):
    """
    This class creates representations of image patches.

    The image patches should be created similar to what extractPatches does.

    """

    def __init__(self, encoder, device):
        """

        Parameters
        ----------
        encoder: torch.nn.Model
            Encoding network
        """
        self.encoder = encoder
        self.encoder.eval()  # Eval mode
        self.device = device

        # Avg Pooling for the linear classifier
        self.avg_pooling = nn.AdaptiveAvgPool2d((1, 1))

    def __call__(self, sample):
        image = sample['image'].type(torch.FloatTensor).to(self.device)

        # Extract dimensions
        batch_size = image.shape[0]
        n_patches_x = image.shape[1]
        n_patches_y = image.shape[2]
        patch_size = (image.shape[4], image.shape[5])

        # Merge batch- and image-dimension
        # And get encoded representation
        image = self.encoder(image.view((-1, 3) + patch_size))
        enc_size = image.shape[1]

        # Pool to a single dimensional representation of the images
        image = self.avg_pooling(
            image.view((batch_size, n_patches_x, n_patches_y, -1)).permute(0, 3, 1, 2)
        ).reshape(batch_size, enc_size)

        return {'image': image, 'label': sample['label']}


class AutoencodeImages(object):
    """
    This class creates representations of image patches.

    The image patches should be created similar to what extractPatches does.

    """

    def __init__(self, encoder, enc_size, device=None):
        """

        Parameters
        ----------
        encoder: torch.nn.Model
            Encoding network
        """
        self.encoder = encoder
        self.encoder.eval()  # Eval mode
        self.device = device
        self.enc_size = enc_size

    def __call__(self, sample):
        image = sample['image'].type(torch.FloatTensor).to(self.device)

        return {'image': self.encoder(image).view(-1, self.enc_size), 'label': sample['label']}


def training(net, num_epochs, data_loader, print_every=200,
             max_runs=None, device=None, one_batch=False,
             criterion=None, optimizer=None, transform=None, tqdm=None):
    """
    Trains Classifiers on ImageNet Data.

    Parameters
    ----------
    net: nn.Module
        Neuralnetwork to be trained
    num_epochs: Int
    data_loader: torch.utils.data.Dataloader
    print_every: Int
        Average Loss gets printed every %print_every%
    max_runs: Int
        Iterates only max_runs times on data_loader
    device: CUDA Device
    one_batch: bool
        Training on a single batch for tests
    criterion: Loss-Function
        Defaults to nn.CrossEntropyLoss()
    optimizer: Network Optimizer
        Defaults to optim.SGD (Stochastik gradient descent)
    transform: Callable
        Transforms data before processing

    Returns
    -------
    all_losses: List
        List of all losses

    """
    # Defaults
    criterion = criterion or nn.CrossEntropyLoss().cuda()
    optimizer = optimizer or optim.SGD(net.parameters(), lr=0.001)

    if max_runs is None:
        max_runs = len(data_loader)
    else:
        max_runs = min(max_runs, len(data_loader))

    running_loss = 0.0
    all_losses = []
    _sample = next(iter(data_loader))

    if tqdm is not None:
        data_iter = tqdm(
            enumerate(data_loader, 1),
            desc="Learning..",
            total=max_runs
        )
    else:
        data_iter = enumerate(data_loader, 1)

    for epoch in range(num_epochs):
        for i, sample in data_iter:
            if one_batch:
                sample = _sample

            if transform:
                sample = transform(sample)

            image = sample['image'].type(torch.FloatTensor).to(device)
            label = sample['label'].to(device)

            # Gradient wird zu Beginn eines Durchlaufes wieder auf 0 gesetzt, da er sonst akkumuliert
            optimizer.zero_grad()
            output = net(image)
            loss = criterion(output, label)

            # berechnet aktuellen Gradienten
            loss.backward()

            # passt Parameter entsprechend aktuellem Gradienten an
            optimizer.step()

            running_loss += loss.item()
            if i % print_every == 0:
                avg_loss = running_loss / print_every
                all_losses.append(avg_loss)

                if tqdm is not None:
                    data_iter.set_description_str("Last avg loss  {:.5f}".format(avg_loss))
                else:
                    print("[%d, %5d] loss: %.3f" % (epoch + 1, i, avg_loss))

                running_loss = 0.0

            if i == max_runs:
                break

    return all_losses


def validation(net, data_loader, device=None, transform=None, tqdm=None):
    """
    Validates on imagenet Dataset

    Parameters
    ----------
    net: nn.Module
        Neuralnetwork to be trained
    data_loader: torch.utils.data.Dataloader
    device: CUDA Device
    transform: Callable
        Transforms data before processing

    Returns
    -------

    """
    correct = 0
    total = 0

    if tqdm is not None:
        data_iter = tqdm(data_loader)
    else:
        data_iter = data_loader

    # no_grad: beschleunigt Berechnung bei Evaluierung
    with torch.no_grad():
        for data in data_iter:
            if transform:
                data = transform(data)

            image = data["image"].type(torch.FloatTensor).to(device)
            label = data["label"].to(device)

            outputs = net(image)

            _, predicted = torch.max(outputs.data, 1)

            # label.size(0) entspricht batch size
            total += label.size(0)
            correct += (predicted == label).sum().item()

        print('Accuracy of the network: %d %%' % (100 * correct / total))
        print(correct)
