"""
Contains a example routine for learning from image patches.

For a more intuitive example check out the cpc_example.ipynb notebook.

"""
import torch
import torch.optim as optim


def train(dataloader, rnn, resnet, criterion, classifier, class_criterion, same_batch=False,
          device=None, optimizer=optim.Adam):

    cpc_optimizer = optimizer(list(rnn.parameters()) + list(resnet.parameters()) + list(criterion.parameters()))
    class_optimizer = optimizer(classifier.parameters())

    # Get patch dimensions
    test_data = next(iter(dataloader))
    batch_size = test_data["image"].shape[0]
    n_patches_x = test_data["image"].shape[1]
    n_patches_y = test_data["image"].shape[2]
    patch_size = (test_data["image"].shape[4], test_data["image"].shape[5])

    for i, data in enumerate(dataloader, 1):
        if same_batch:
            data = test_data
        image = data["image"].type(torch.FloatTensor).to(device)
        label = data["label"].to(device)

        # Merge batch- and image-dimension
        img_patches = image.view((-1, 3) + patch_size)

        enc_patches = resnet(img_patches)

        # Concatenate all patches (x- and y-dimension)
        enc_patches = enc_patches.view((batch_size, n_patches_x * n_patches_y, -1))

        # Apply rnn
        outputs, hidden = rnn(enc_patches)
        cpc_optimizer.zero_grad()
        loss, all_losses = criterion(enc_patches, outputs)
        print("Loss: ", loss.item())
        loss.backward()
        cpc_optimizer.step()

        # Teach Linear Layer for Validation
        out = classifier(enc_patches.view(batch_size, -1).detach())
        class_optimizer.zero_grad()
        class_loss = class_criterion(out, label)
        class_loss.backward()
        class_optimizer.step()

        print("NN-Loss: ", class_loss.item())
