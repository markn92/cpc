"""
This module contains methods to create a subset from
the given hdf5 imagenet dataset.

This subset is created by taking the top n labels.

Result is a list of indices that hava data of those labels and
can be used as a mapping in da pytorch dataloader.

This yields train and test indices for the top 100 labes of the train set:

    >> python get_imagenet_subset.py 100


"""

import numpy as np
from tqdm import tqdm as tq


def get_n_max_labels(data, n):
    """
    Returns the n maximum lables within a targets dataset.

    data: Dict
        With "targets" field.
        data["targets"][idx] = label_number

    n: Int
        Number of maximum lables

    """
    count = np.zeros(int(np.max(data["targets"])) + 1)
    for index, label in enumerate(tq(data["targets"])):
        count[label] += 1
    return (-count).argsort()[:n]  # n max lables


def create_index_map(data):
    """
    Creates a mapping from label to list of indices of
    the target data that has this label.

    Returns:
    --------
    map_label_index: Dict
        label -> list(indices)
    """
    map_label_index = {}
    for index, label in enumerate(tq(data["targets"])):
        try:
            map_label_index[label].append(index)
        except KeyError:
            map_label_index[label] = [index]

    return map_label_index


def get_labels_indices(map_label_index, labels):
    """
    Returns a list of indices of data having lables within lables.

    """
    indices = []
    for label in labels:
        indices += map_label_index[label]
    return indices


def create_labels_map(label_list):
    """
    Creates a map old_label -> new_label

    """

    labels_map = {}
    for i, label in enumerate(label_list):
        labels_map[label] = i

    return labels_map


if __name__ == '__main__':
    import h5py
    import pickle
    import tqdm
    import sys

    try:
        number_labels = int(sys.argv[1])
    except (IndexError, ValueError):
        print("Give number of labels as an argument.")
        sys.exit(1)

    # Read original dataset
    f = h5py.File('/home/data/imagenet/imagenet64.hdf5', 'r', libver='latest')

    # Get top n labels
    print("Find top {} labels..".format(number_labels))
    top_n_labels = get_n_max_labels(f["train"], number_labels)

    # Get all indices that have these labels from
    # train and test set
    print("Get all indices of those labels for the training set..")
    train_indices = get_labels_indices(
        create_index_map(f["train"]),
        top_n_labels
    )
    print("Get all indices of those labels for the validation set..")
    test_indices = get_labels_indices(
        create_index_map(f["validation"]),
        top_n_labels
    )

    # Write the indices down to files
    train_path = "./top-{}-lables-indices-train.pickle".format(number_labels)
    print("Training indices are written to ", train_path)
    with open(train_path, "bw") as file:
        pickle.dump(train_indices, file)

    test_path = "./top-{}-lables-indices-test.pickle".format(number_labels)
    print("Validation indices are written to ", train_path)
    with open(test_path, "bw") as file:
        pickle.dump(test_indices, file)

    labels_map_path = "./label_map.pickle"
    print("Labels map is written to ", labels_map_path)
    with open(labels_map_path, "bw") as file:
        pickle.dump(create_labels_map(top_n_labels), file)
